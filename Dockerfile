#Use official node image as the base image
FROM node:latest as build

#Pass in the arguments to the Dockerfile
ARG GIT_TOKEN
ARG GIT_PASSWORD

#Set the working directory
WORKDIR /usr/local/app

#Add the source code to app
RUN git clone https://${GIT_TOKEN}:${GIT_PASSWORD}@gitlab.com/LudicrousJeeves/spa-resume.git

#Sets the new working directory
WORKDIR /usr/local/app/spa-resume

#Install all the dependencies
RUN npm install

#Generate the build of the application
RUN npm run build

#Use aws-cli image as the base image
FROM mesosphere/aws-cli

#Copy the build output
COPY --from=build /usr/local/app/spa-resume/dist .

#Push to S3 bucket
CMD ["s3", "sync", "./", "s3://resume.j33v3s.com", "--delete"]