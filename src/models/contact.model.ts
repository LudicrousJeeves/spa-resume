export interface Contact {
    Type: string;
    Image: string;
    Value: string;
}
