export interface Project {
    Title: string;
    Details: string[];
    Git: string;
    Url: string;
    Status: string;
}
