export interface Job {
    CompanyName: string;
    Title: string;
    Image: string;
    Start: string;
    End: string;
    Description: string;
    Tasks: string[];
}
