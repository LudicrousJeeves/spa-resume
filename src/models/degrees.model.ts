export interface Degree {
    DegreeType: string;
    Major: string;
    School: string;
    GraduationDate: string;
    Website: string;
    Image: string;
}
