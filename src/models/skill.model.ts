export interface Skill {
    Name: string;
    Level: number;
}
