export interface About {
    Title: string;
    Description: string;
    Content: string;
}
