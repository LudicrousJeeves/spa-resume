import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Job } from 'src/models/job.model';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  jobs: Job[];
  currentJob: any;
  index: number;

  constructor(dataService: DataService, private route: ActivatedRoute, private router: Router) {
    this.jobs = dataService.jobs;
  }

  selectJob(event: any)
  {
    this.router.navigateByUrl('/home?experienceId=' + event.index);
    this.currentJob = this.jobs[event.index];
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.index = params['experienceId'];
      this.currentJob = this.jobs[this.index];
      if(this.index == undefined)
      {
        this.index=0;
      }
      if(this.currentJob == undefined)
      {
        this.currentJob = this.jobs[0]
      }
    })
  }
}
