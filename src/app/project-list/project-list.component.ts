import { Component, OnInit } from '@angular/core';
import { Project } from 'src/models/project.model';
import { DataService } from '../data.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  projects: Project[];
  currentProject: Project;

  constructor(private dataService: DataService) {
    this.projects = this.dataService.projects;
   }


  setSelectedProject(id){
    this.dataService.currentProject.next(id);
    console.log(this.currentProject);
  }


  ngOnInit(): void {
    this.dataService.currentProject.subscribe(project => this.currentProject = this.dataService.projects[project])
  }

}
