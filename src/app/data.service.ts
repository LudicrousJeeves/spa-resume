import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Degree } from 'src/models/degrees.model';
import { Job } from 'src/models/job.model';
import { Skill } from 'src/models/skill.model';
import { Contact } from 'src/models/contact.model';
import { Project } from 'src/models/project.model';
import { About } from 'src/models/about.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  jobs: Job[];
  education: Degree[];
  skills: Skill[];
  contact: Contact[];
  projects: Project[];
  about: About[];
  currentProject = new BehaviorSubject(0);

  constructor() {
    this.jobs = [
      {
        CompanyName: "Choctaw Nation",
        Title: "Systems Engineer III",
        Image: "../assets/choctaw.png",
        Start: "November 2020",
        End: "Current",
        Description: "Automate Daily Operational Tasks. Implement new applications to our health system. Perform Database backups and restorations.",
        Tasks: ["Script Daily Operations to automate and regulate daily tasks for a health system", "Perform database backups and restorations for a B-Tree database.","Verify integrity of B-Tree database.",
        "Implement new products to work with our health system by standing up application and database servers", "Develop .NET class libraries to work with our health system.",
        "Set up SSL/TLS for IIS applications and Intersystems Web Servers", "Write Intersystems Objectscript (MUMPS) routines to perform cleanup tasks"
      ]
      },
      {
        CompanyName: "Choctaw Nation",
        Title: "Systems Engineer II",
        Image: "../assets/choctaw.png",
        Start: "May 2019",
        End: "November 2020",
        Description: "Setup servers. Help with migrations. View how workflow happens and try to optimize it.",
        Tasks: ["Setup High Availability for a medical record system spread over 13 clinics", "Remotely deploy applications and updates through an End Point Management application",
        "Stand up application and terminal servers",
        "Make sure that all workstations are patched and up to date through the use of WSUS", "Setup automative workflows with Powershell"
      ]
      },
      {
        CompanyName: 'Choctaw Nation',
        Title: 'IT Technician',
        Image: "../assets/choctaw.png",
        Start: 'May 2018',
        End: 'May 2019',
        Description: 'Basic troubleshooting. Handle AD. Setup and train new users.',
        Tasks: ["Added users to Active Directory", "Managed users access to applications through group management on Active Directory",
        "Troubleshoot common technical problems with over 1500 users in the domain",
        "Trained the end user to make sure they know how to use our intranet site and applications",
        "Make sure that server closets were clean and followed appropriate protocols", "Handled orientation and initial user login"
      ]
      },
      {
        CompanyName: 'Choctaw Nation',
        Title: 'Hotel Front Desk',
        Image: "../assets/choctaw.png",
        Start: 'June 2017',
        End: 'May 2018',
        Description: 'Checked in guests and handled large sums of money.',
        Tasks: ["Make sure guests are handled appropriately and professionally", "Check in multiple party blocks with limited staff",
        "Verify that all rooms were paid for appropriately",
        "Help managers with defusing a tense situation", "Help coworkers in understanding our Oracle system better and make sure that everything is being done properly"
      ]
      }
    ];
    this.education = [
      {
        DegreeType: "Bachelor's Degree",
        Major: "Computer Science",
        School: "Southeastern Oklahoma State University",
        GraduationDate: "December 2018",
        Website: "https://www.se.edu/ccps/computer-science/",
        Image: "../assets/southeastern.jpg"
      },
      {
        DegreeType: "Associate's Degree",
        Major: "General Studies",
        School: "North Central Texas College",
        GraduationDate: "May 2016",
        Website: "https://nctc.edu",
        Image: "../assets/nctc.png"
      }
    ];
    this.projects = [
      {
        Title: "Poster Board",
        Details: ["Made using the MEAN stack","Uses MongoDB to store all posts and user information", "Uses ExpressJS to help with the REST API structure on the backend", "Front end built in Angular 8",
        "Uses NodeJS for dealing with the backend API calls", "Uses Multer to verify file type for image uploading", "Uses JWT for user authentication and authorization", "Uses Bcrypt for hashing the passwords",
      "Uses Mongoose to assist in connecting to the MongoDB database and to create schemas for the posts and users", "Hosted on AWS using elstic beanstalk for the backend and S3 for the front end."],
        Git: "https://gitlab.com/LudicrousJeeves/mean-example",
        Url: "https://posterboard.j33v3s.com",
        Status: "Active"
      },
      {
        Title: "SPA Resume",
        Details: ["Uses Angular 8 for the front end", "There is no database attached to the website", "All information is stored in a data service as arrays of JSON objects",
        "Uses Material for the front-end styling", "Hosted on AWS using a static S3 bucket", "Established a CI/CD pipeline on GitLab to build a docker image and push to the S3 bucket hosting this site. This happens only on merge to the master branch."],
        Git: "https://gitlab.com/LudicrousJeeves/spa-resume",
        Url: "",
        Status: "Active"
      },
      {
        Title: "WIT",
        Details: ["Project developed for a company called Women In Trucking", "Kiosk application using Electron.NET", "Uses .NET MVC (Model View Controller) structure", "Uses Bootstrap 4 for front-end styling", "Information is stored in multiple different JSON files for the customer to easily update information in the application"],
        Git: "",
        Url: "",
        Status: "Active"
      }
    ]
    this.skills = [
      {
        Name: "C++",
        Level: 3
      },
      {
        Name: "C#",
        Level: 2
      },
      {
        Name: "HTML/CSS",
        Level: 3
      },
      {
        Name: "MVC",
        Level: 2
      },
      {
        Name: "Ionic",
        Level: 3
      },
      {
        Name: "Angular",
        Level: 3
      },
      {
        Name: "NodeJS",
        Level: 2
      },
      {
        Name: "Powershell",
        Level: 4
      },
      {
        Name: "Intersystems",
        Level: 4
      },
      {
        Name: "Git",
        Level: 3
      },
      {
        Name: "Docker",
        Level: 3
      },
      {
        Name: "CI/CD",
        Level: 2
      },
      {
        Name: "Javascript",
        Level: 3
      },
      {
        Name: "AWS",
        Level: 3
      }
    ];
    this.contact = [
      {
        Type: "Email",
        Image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA81BMVEX////n5NfnWk339e23tq2yOS/n6t23urHoVEayMynq59rnWEv18+n39u74/va1tKu3cGfqUUPmSDm2dWy0PzXmTD7OzMGzvLPf3NDBwLboX1LmRzi9vLLV08fX1cn53tz16+LocGTysazurKPwx77rmY7nZ1rqjoTutavy2dD15NzrfHPYV0zFTELqhXry0cjNnJLoxbnTkIbooZXozsK/sajokYbZhXqsHguvLSD76Of0vbn1xcLpdGj41tTwopzOUkfWjYPEq6Lot6vRmY/Io5ro2MzCraTbf3TeeG3oq5+8W1TFdXDetbOrFADfuLXAZ2GpfDPRAAAJKklEQVR4nO2de3/TNhSGE5M0roW9OTShcdY7LZRw6bitSdaWDdjGgML3/zST7bjxRZJ1bN2yn97/aBNbj8+xXp0jF3c6VlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWTbRYns7OZvf8eyp0X7j8Qzz60+WCynccDUPXRS46D5QgilbwEo8dueHw7JjIdzJykZMITU8CNWEUKT94PcwAwtFJhe8AhatfxxrN7m8aon94FK4BUBgdFAGX0xwfluu+3KhM9YNHbpEATX8vZOjUKQlNX29QpvrB6RCVEabL3BQzLP8WK7w63BTE4OmZSyBYI15XIphmargZmeoHH0aVAKaI2b3okn/voOHpBmSq7z+mADooSgFvSBFeZeqR8ZkaPI0Y409MY0G6Ce+uwuiR0ZnqB09oAUyHXxNCx/RM9e+9GLFG77jHjLvw7kNnT01FDM4dZnjSO/GAfRGcNFNNZPSDk6oJljVcdI5rrkKMOHznm4fo35/VBgdn4LLzuJ4wyVTTJpzgpcs18tPOVW2gY6Hh0qhMzRUSNQOfdSKez2GNHhuUqcVCgkl41uEExPGOTMlUXEiwTLBIGPET4kx9YkSm+v47vgxNBCHEmfpCUQuHCXhILCTEEDr6WziQDG1CiDNVbwuHUUgIItTcwqGUumIJcabqKoxxIQGYYhKh6DmcEGeqnhaO79cUEgQ96+80INTUwqkvJKp63m9IqKHZyFVIlBTt9BsT4ky9UZqpfIVEUe/7/RaEils48Y4EbHh4ium3JHRQqKqFgwuJKTRDn+30WxPGLRwl5YZ/eMVZSKy1CmBLwrgwVpCpwSOuUjevaKcviFBBC4e4I1Gj9/2+MELpLZwAVkjEI8oHUACh3BYOuJBw4lVMXzAhzlRZLRxcSEAzNHreF0+IEeW0cJg7EmQ92ykDCiGU08JpUEgUpxihhBJaOLU7EtUxRNUAsglhF1BwC6dBIUHI0BrCK9gpXEdcCwdeSCBUmWLqCJG7xdlVvvuGsEwFFxIo/PzzdgPCrTfAuWwk5nkxzh2J/FgvHjYi7HoesLElotnIvSOxPuvV28GgGWG3u/Ur7HK2bzby70hkpwxfPez1GhN2t7ozWBhHV22ajXiZFgIDePZ20GtDiBmfwM7ZpoUDLiRQ+AcOYEvC7tZfsLxp/rwYuJBwoz9TwHaEXW/rNSxTm7Vw6I820YYY/jbo9UQQ4jC+gV3cJpkK3pFwo08Pe6IIcRghW3ZNnsKBFhIo/Kd3F0EBhLFvwPp5sEwFFxIIXawDKIaw63VfgLIIAZ5sBBcS4ed8AAUR4jB+qHt8qngE7kwNzmH5gdyPhQAKIwT7Bl+zERcSsH6ve/R20JNDiBn/hWUqR7PRP4QVEukqTRoh1DfQ8F0NIrSQcM/+rgRQKCH2DdjCit1s9IMbmAtlqzSJhLFvgJwLDT9QwwgtJNarNKmE2Ddgqw9asxFaSGCTH5AyVDxh4huQMJKbjX4AXCc5nygBlECYlI2QwRFaOMAHR1DF5OUSgn2j3GyEFhLlVZoCQqj9F5uNcSEB+LLjXrECKIkQWjbmWzjBOWw6rq7SlBDG9g8aaPa8GLSQIK3SFBHGZSMkjGmzEVhIkFdpqgihZWOcqcBCgrJKU0eIfQNUNoYz0I5EvhWjizC2f8jKBBZA6ipNKSH2DfjzLjxirdLUEoLtnxMQMVZpqglxGMHP7tbxsVdpygnjslFoGOMNM34+FYSxb8AfcKUq3jCDACohBJeNdKGwdpWmhRBcNtK02jAzkRBaNpJPTGvFGEEowDfciGuVpo8Q3DUunZV3laaREL7bmFNhw8xYQnDZuD4lYJXGT9iXQAgtG7MzQlZpmgnhu40OYcPMbEJo2QhepeknBJaNPK0Y4wgBvsHXijGPMPYNro4FZyvGQEK+3UZs8m0DqJGQo2zkb8WYSVjnG21M3hRC5lMqzA2zjSGkP6UCbMUYTEhpN7Y1eZMIiWUjuBVjNmGl3digFcMmfKCbsOQbTVoxphPmy8ZmrRg2ofYsjZX9cYOIVZqZhOkfNzRuxbAJTcjSBLE7a1PJbwChtzve+0kCoDGE3njP83Z/kcBoBqG3O9n1Ek7xiEYQentjL0PtiWY0gjAN4Eqiw6ifEAew+O/9/xvhZN+r/EhkGDUTevsT0k9F+oZmQuwRRPCuON/QSYg9oksE7Ir0DY2E3nhM40vwBYVRG2Fm8qwrIARRFyE7gNlFEGH/egjrA7iSAN/QQuiNSR5B/OR+a0QNhB7J5BkfbsmontDb4w1g9vl2iOoJJ2STZyC2s3/FhHiVxjfFFL/VxjcUE07qPYKI2ML+VRLicTYI4EqNw6iQkMfkGd/eb2j/yghbBTBVM99QRchv8oxjNPINNYQ4gPwmzzhMkzAqIRQRwOxIYEQFhNix20wxpYOBfUM+obfXeoopHg8YRvmEDU2egQgrGyUTYhcTGsCVIBOOZMKJqCmmKEjXWCYhTicRHkE8NH8YJRKK8wji0XntXxohDiC0EAQicpaNsgi98S/Ubq8wRi7fkEOIAyjaIyinqWeUQuiNpXgE8Uy1iBIIha7S6lRfNoon9PZUBXClGt8QTyjJ5Omq6RoLJsRJI8vk6WLbv2DCiXyPIIk14YgkVOQR5FNT7Z9FSHn/IZVQmUeQRA0jnXCH9g5LMqHGAN4NgMhIJ/xCew8pkVCdyTNEnHCohNtfae+SJRDi66faI0gilo1UwvlH2vuAq4T4HlDvESSRfINO+I32TucyIV6lmRDAVNWykUp4eUB7L3eJEB9TbiEIU6VspMeQ+m71EqEmk6er5Bs0QjzR0F6unifEa0K9HkFS0TdohJfnMSHxTswTTgzwCILyYaQQbn9JADvXUwaht2tgAFPlykYK4e0iJewcV2/FjBBfKCMDuFLmG2TCy2+dTCcVxJTQEJOnK/MNIuHtx85ax+X/cDohNMbk6VrZP4Fw+/Z7J68DpzjfYEJsOqZ5BEnJhFMhnN9+XXRKuhnlGTGhWSZPV1w2Fgi355fzVxW+JFWjYeiiTIZ6BEneePBgvo01n88vL+c/Lq5JeIkWy9PZUZRq7G0KYBzGBztffvz4+uri+zUxeFZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVhz6DwWBBwzCljjHAAAAAElFTkSuQmCC",
        Value: "everettjordan55@gmail.com"
      },
      {
        Type: "LinkedIn",
        Image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEUAd7f///8AcrUAbrMAdLbA2+uRuNex0eY+kcR3rNIAc7V5qdCty+EAcLQAbLLf7fX3+v1opc4eg71fncq51eg7jsKew94Aernm8vj3/P3J3u3Z6PKjyOEaf7uLudnu9/tNmMjR4e5bnMqXv9wviMCDstU8j8OHsdRim8i21OcBZiGFAAAHVUlEQVR4nO2dW3uyvBKGQxIt5SUgCCLuUEv7rf7/P7igllaRzRBIdXLlOepBA7nNbpgkM8S60SKaB3aYEJxKQjuYR4tbJHL1d+ouOaMOf3RFR4g7lPGlmzYTRjlzHl3DSeSwPGog9GOBufFuxUXs1wlnVI/2q+TQ2S3hij26SpOLra4JV96j66NA3uqXcKZfC5Zis4rQp4+uiyJR/5sw1muS+ZUTXwgj8eiaKJOISsI012cdrIvnaUHo6jnNXMTcgnCp6ygs5SwtstC3j5biCxLp3EmLbhqRua6L4UV0TgKdh2ExEANiaz4ObRI+ug6KFRKsPhmodOczahQv9Og6KBMXnpeEcRwmHtPEL3ctLvhxtancrVt3SYRWkJzZbnbrTLaiM9XHIGL2xmrQNmB6jEmHu018pTZ7HTwE7Fjvn9cK8COy9w6+Qjvsg1G8dQOid2WxQx9ggYjZZS5e+wGLjorXUcBjCGAx3aAdi9Tvpyu1CJGuiwIwCC9COtvwfdrP9q0jykYUKzCgtUE5nzI4oGXFCBuRglaKSjuEI5GdhhBmCNswWfRzXQnfXOMsBwFaB3SrPgUvhhfh2+MRL8MIt+ja0NsOI1yjcy8LoE1aKds/usZDBbW6K6XaE+JrQzZwHGboxiFrdJG2y390hQdryJdFqRM6w3SY4Y3R9Ib6aCohPPPAulzdd0oRumrYbgjhCZ1ZWp7sG0L4js4sJcMs0xQjIHHOcMIDupn0S+IEBcweXVVJcRtKiNatz3q31i6a4eyjpWDGaZbgWwsr8WQNIMzxmTO/4mG/ZXPEOggvcsKeL+EMOWDZUTvHop9jByzPe3V4Tl9Q36r9kdi3NOPWRmhvN4p7x9n9bulJl0NfX+IsP5xumm8VC/wj8EacUnJ8d1+i2ac7PyZUo5vRV+JUCMYEo1rSGRkZGWkpXa92lCFYiuUoSfZhGO73CRFMUMWsxTvb1P7t21GovZTD2P4jOETbrb/OSjsxTTN/u9m9nkOmzoQS4vivVWfSbHtzLw/aSy33TaUoc5a7dcsXd+rv/iWeEkja4416aXLS0LzHm+ySWikuyPusu4xlbV7J9MY+7/VFrfd3L3WOfYUs/2bLmIt8BzrqmbrxxIyQfdL7k5cUsG31cuWBZHHUX6BSFE/7UQp550ftV6U9VzQu+jnZ4HgDt5t3E046TgB546rmEIYdp6q2VMUZ4rG80WK6mzpiDnnhS+19rHfKKPV6aQg2sAEv2k31AUdVE3IH9L/3Ok3kZ1dNyPcDT+38ah1O4mlXTAjbNGhRdr9KPR+hkG7BUv4UxqpaQiE5BittJlg0lBJ6gw57NOlz/KKhlHDAMYE2jT8BopLwfdCJpGalo2cblYTDLju0aPRtJJWE0+h1ZD99fsKxB3efn3DsWSUEhCMPKyEgHLliYCDMRk2nGAjHBUZEQTgb04goCBdjFgwUhKNWfRyEY86Y4yBcjLC/cRBaZ/nZFAnhiPs6f0yY+Vupj8YtBsL1Kgi9LxF7vhn67Si/XvwV4eZM2c8pR069/duwtpQPBvA3hOtjPX4fF0lrBLUmyQcL/hPCz8Zdb+8Mjx0zwun2F4RtuSfEBxzRlzZN/4CwPbkG7d9J/tETt2FXcg0B2mn9knR2AOWE687Xe+Db1tLfiMoJz52TIA+hz/lPdjJVTdiXAIZ9Ah/0JjsQVRP2DR/wdWvp5UIxYb9PHnoF8iS7XCgm7De2YAdXihnrOQkBQXvKRCoQZc/ZSyEOeQ+20y8dakwtYf0sVZMEzAKX3qBRSriGrGHAgbiQTfKglBDkewDetpZO6qSU8B/IDuGgZ0nHbVRKCPvZPRih7Fe+0n18mB8XOJn+7wkJgZ5qYIBK2fA4KgnrpdoIYV3+GQnr525bBFwQn5EQuDuNmBC4J2YIhz3NED6CkGlPaNrQEBpCQ2gIDaEhNISG0BAaQkNoCA2hITSEhtAQGkJDaAgNoSE0hIbQEBpCQ2gIDaEhNISG0BAaQkNoCA2hITSEhtAQGkJDaAivCUH3GzETwlICv9WeDourDwz0AMxiL534nEKuUtcvEsISzwNv7nJQhBP5xOe0I7tjpc+7a3asJ7NnKeDFrqIKkPAf4Kc1PL+3x53uC/G4t+kbSrWIh/13LEcE+yraI+h8QXZoysPgJN1JR9LDgImB857JJh0badfbf9gt+gjb0vcwErcVsu2cDZv5BM3bH2bnYnxodt4uqULDZ4Wuh+mZ2MvoVyPjfT+9EiIbTgKLQmLrPUy5TaTNORxyAiIfUhGF6JxEuiTWbhaLyELzcbgg40K2P7ucpUUsV+duytyCUDr8EAKVodCIZUWTJdp7OoniE65M4BjrOhKdMuhiSejruiRS/5uwN8QmUl18mpc0o+2hfBHrO7npdyLVlX6tWCX/rFLFzjrST2OUQyu3+08yXD8W+qyLXMQ/DturdL9RzvRoR4flV67M64TGqbvkjDqYm7JMPM+X7rU7upayeRHNAzvE6rtJQjuYR7XEEf8Hv9yYWp88rfoAAAAASUVORK5CYII=",
        Value: "https://www.linkedin.com/in/jordan-everett-093B09155"
      },
      {
        Type: "Git",
        Image: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Git_icon.svg/1200px-Git_icon.svg.png",
        Value: "https://gitlab.com/LudicrousJeeves"
      }
    ];
    this.about = [
      {
        Title: "My Past",
        Description: "My education history",
        Content: "Hello! My name is Jordan Everett and I am a college graduate from Southeastern Oklahoma State University where I received my Bachelor's degree in Computer Science with a minor in music."
      },
      {
        Title: "My Present",
        Description: "My work history",
        Content: "I'm currently a Tier 3 Information Systems Engineer primarily working with a B-Tree database for a healthcare system. My primary responsibilities in my current position is to establish new application servers, make sure that our primary health database is in a good state, and maintain and standardize the use of good and modern security protocols throughout our domain."
      },
      {
        Title: "My Future",
        Description: "My goals",
        Content: "My active career goal is to switch paths into the software development world. I would like to become a full stack developer since I'm familiar with both front end and back end development. I have done some development work in my current position using Intersystems Objectscript, otherwise known as MUMPS, on the back end and C# for the front end. I have also worked on developing a REST API using Intersystems Objectscript and developing an Angular front-end application to coordinate with it."
      }
    ]
  }
}
