import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit{

  @Input('rating') rating: number;
  @Input('tooltip') tooltip: string;

  constructor()
  { 
  }

  ngOnInit() {
  }

}
