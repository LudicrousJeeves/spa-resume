import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Project } from 'src/models/project.model';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})

export class ProjectsComponent implements OnInit {
  projects: Project[];
  currentProject: Project;
  index: number;

  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute, public dialog: MatDialog) {
    this.projects = dataService.projects;
  }

  selectProject(event: any){
    this.router.navigateByUrl('/projects?projectId=' + event.index);
    this.currentProject = this.projects[event.index];
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.index = params['projectId'];
      this.currentProject = this.projects[this.index];
      if(this.index == undefined)
      {
        this.index=0;
      }
      if(this.currentProject == undefined)
      {
        this.currentProject = this.projects[0]
      }
    })
  }

}
