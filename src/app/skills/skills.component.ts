import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Pipe, PipeTransform } from '@angular/core';
import { sortBy } from 'lodash';
import { Skill } from 'src/models/skill.model';

@Pipe({ name: 'sortBy' })
export class SortByPipe implements PipeTransform {
  transform(value: any[], order='', column: string): any[] {
    if(!value || order === '' || !order) { return value; }
    
  }
}

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  skills: Skill[];
  data: any;

  getToolTipText(skillLevel:number){
    return this.data.getSkillLevelDescription(skillLevel);
  }


  constructor(dataService: DataService) 
  {
    this.data = dataService;
    this.skills = dataService.skills;
  }

  ngOnInit() {
  }

}
