import { Component, OnInit } from '@angular/core';
import { Project } from 'src/models/project.model';
import { DataService } from '../data.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {
  currentProject: Project;
  projects: Project[];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.projects = this.dataService.projects;
    this.dataService.currentProject.subscribe(project => this.currentProject = this.dataService.projects[project])
  }

  

}
