import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { About } from 'src/models/about.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  about: About[];

  constructor(private dataService: DataService) {
    this.about = dataService.about;
   }

  ngOnInit() {
  }

}
