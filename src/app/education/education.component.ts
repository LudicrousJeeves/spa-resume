import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Degree } from 'src/models/degrees.model';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  degrees: Degree[];

  constructor(dataService: DataService) {
    this.degrees = dataService.education;
   }

  ngOnInit() {
  }

}
