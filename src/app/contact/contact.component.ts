import { Component, OnInit, Inject } from '@angular/core';
import { DataService } from '../data.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Contact } from 'src/models/contact.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contact: Contact[];

  constructor(private dataService: DataService, private modalService: NgbModal) {
    this.contact = dataService.contact;
   }
   
  goTo(contact) {
    if(contact.Type!=="Email")
    {
      window.location.href = contact.Value;      
    }
  }

  ngOnInit() {
  }

}
